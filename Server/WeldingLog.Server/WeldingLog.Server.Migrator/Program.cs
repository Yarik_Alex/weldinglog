using FluentMigrator.Runner;
using WeldingLog.Server.Migrator.Migrations;

var builder = WebApplication.CreateBuilder(args);

var current = Directory.GetCurrentDirectory();
var parent = Directory.GetParent(current);
var dir = parent?.GetDirectories();

var root = new ConfigurationBuilder()
    .SetBasePath(dir?.OrderBy(x => x.FullName.Length).ToArray()[1].FullName)
    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .Build();

var connectionString = root.GetConnectionString("Database");

builder.Services.AddFluentMigratorCore()
    .ConfigureRunner(
        rb => rb
            .AddPostgres()
            .WithGlobalConnectionString(connectionString)
            .ScanIn(typeof(Init).Assembly)
            .For.Migrations())
    .AddLogging(lb => lb.AddFluentMigratorConsole());

var app = builder.Build();

app.Services.CreateScope()
    .ServiceProvider.GetRequiredService<IMigrationRunner>()
    .MigrateUp();